package no.uib.inf101.grid;

import java.util.Iterator;
// Damn I hate creating iterators..

public class GridIterator<T> implements Iterator<GridCell<T>>{
  // Field variables
  private final Grid<T> grid;
  private int row;
  private int col;
  
  // Iterator constructor
  public GridIterator (Grid<T> grid) {
    this.grid = grid;
    this.row = 0;
    this.col = 0;
  }
  
  @Override
  public boolean hasNext() {
    if (this.row < grid.rows() && this.col < grid.cols()) {
      return true;
    }
    return false;
  }
  
  @Override
  public GridCell<T> next() {
    CellPosition cellPos = new CellPosition(row, col);
    GridCell<T> cell = new GridCell<>(cellPos, grid.get(cellPos));
    this.col++;
    
    // checks if we reach the rows end, in case next row
    if (this.col == grid.cols()) {
      this.row++;
      this.col = 0;
    }
    
    return cell;
  }
}
