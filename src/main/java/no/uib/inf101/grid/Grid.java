package no.uib.inf101.grid;

import java.util.Iterator;

public class Grid<E> implements IGrid<E> {
  // Field Variables
  private final int rows;
  private final int cols;
  private final E[][] grid;
  
  /**
  * Constructor for new Grid
  * @param rows
  * @param cols
  */
  @SuppressWarnings("unchecked") // Java does not enjoy generic instantiation
  public Grid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    grid = (E[][]) new Object[rows][cols];  // casts generic type Grid to chosen type
  }
  
  /**
  * Constructor for new Grid with predefined defaultValue not null
  * @param rows
  * @param cols
  * @param defaultValue
  */
  @SuppressWarnings("unchecked") // Java does not enjoy generic instantiation
  public Grid(int rows, int cols, E defaultValue) {
    this.rows = rows;
    this.cols = cols;
    grid = (E[][]) new Object[rows][cols];  // casts generic type grid to chosen type
    
    // fills empty lists with defaulValue
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        this.grid[i][j] = defaultValue;
      }
    }
  }
  
  @Override
  public int rows() {
    return this.rows;
  }
  
  @Override
  public int cols() {
    return this.cols;
  }
  
  @Override
  public Iterator<GridCell<E>> iterator() {
    return new GridIterator<E>(this);
  }
  
  @Override
  public void set(CellPosition pos, E value) {
    this.grid[pos.row()][pos.col()] = value;
  }
  
  @Override
  public E get(CellPosition pos) {
    return this.grid[pos.row()][pos.col()];
  }
  
  @Override
  public boolean positionIsOnGrid(CellPosition pos) {
    if (pos.row() >= 0  && pos.row() <= this.rows()-1) {
      if (pos.col() >= 0 && pos.col() <= this.cols()-1) {
        return true;
      }
    }
    return false;
  }
}
