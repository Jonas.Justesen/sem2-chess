package no.uib.inf101.grid;

/**
* A CellPosition consists of a row and a column coordinate.
* @param row
* @param col
*/
public record CellPosition(int row, int col) {}
