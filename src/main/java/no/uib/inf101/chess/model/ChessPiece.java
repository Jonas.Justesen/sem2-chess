package no.uib.inf101.chess.model;

import no.uib.inf101.chess.controller.ControllableChessModel;
import no.uib.inf101.grid.CellPosition;

public abstract class ChessPiece {
    private boolean captured;
    private boolean white;
    private char pieceChar;
    private CellPosition pos;
    private ControllableChessModel model;

    public ChessPiece(ControllableChessModel model, boolean white, char pieceChar) {
        this.white = white;
        this.pieceChar = pieceChar;
        this.model = model;
    }

    public boolean isCaptured() {
        return captured;
    }

    public void setCaptured(boolean isCaptured) {
        this.captured = isCaptured;
    }

    public boolean isWhite() {
        return this.white;
    }

    public void setWhite(boolean white) {
        this.white = white;
    }

    public char pieceChar() {
        return this.pieceChar;
    }

    public CellPosition getPos() {
        return this.pos;
    }

    public void setPos(CellPosition pos) {
        this.pos = pos;
    }

    public boolean legalMove(ChessBoard board, CellPosition currentPosition, CellPosition destination) {
        return false;
    }
}
