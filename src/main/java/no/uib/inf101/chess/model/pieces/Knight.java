package no.uib.inf101.chess.model.pieces;

import no.uib.inf101.chess.model.ChessModel;
import no.uib.inf101.chess.model.ChessPiece;


public class Knight extends ChessPiece{

    public Knight(ChessModel model, boolean white) {
        super(model, white, 'n');
    }
    
}
