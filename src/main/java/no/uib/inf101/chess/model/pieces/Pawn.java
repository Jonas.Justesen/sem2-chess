package no.uib.inf101.chess.model.pieces;

import no.uib.inf101.chess.model.ChessModel;
import no.uib.inf101.chess.model.ChessPiece;


public class Pawn extends ChessPiece{

    public Pawn(ChessModel model, boolean white) {
        super(model, white, 'p');
    }

}
