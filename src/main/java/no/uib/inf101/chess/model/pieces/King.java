package no.uib.inf101.chess.model.pieces;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessModel;
import no.uib.inf101.chess.model.ChessPiece;
import no.uib.inf101.grid.CellPosition;

public class King extends ChessPiece{
private boolean hasCastled;

    public King(ChessModel model, boolean white) {
        super(model, white, 'k');
    }

    public void setCastled() {
        this.hasCastled = true;
    }

    public boolean hasCastled() {
        return this.hasCastled;
    }

    @Override
    public boolean legalMove(ChessBoard board, CellPosition currentPosition, CellPosition destination) {
        if(board.get(destination) != null) {
            // check if destination is occupied by another piece of same color
            if (board.get(destination).isWhite() == this.isWhite()) {
                return false;
        }
        }
    
        // check if destination is within bounds
        if (destination.col() > board.cols() - 1 || destination.col() < 0) {
            return false;
        }
        if (destination.row() > board.rows() - 1 || destination.row() < 0) {
            return false;
        }
    
        // check if distance moved is at most 1 x and y
        int rowsMoved = Math.abs(currentPosition.row() - destination.row());
        int colsMoved = Math.abs(currentPosition.col() - destination.col());
    
        if (rowsMoved > 1 || colsMoved > 1) {
            return false;
        }
        return true;
    }
}
