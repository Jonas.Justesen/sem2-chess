package no.uib.inf101.chess.model.pieces;

import no.uib.inf101.chess.model.ChessModel;
import no.uib.inf101.chess.model.ChessPiece;


public class Rook extends ChessPiece {

    public Rook(ChessModel model, boolean white) {
        super(model, white, 'r');
    }
    
}
