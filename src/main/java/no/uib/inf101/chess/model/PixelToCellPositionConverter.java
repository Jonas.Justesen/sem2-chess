package no.uib.inf101.chess.model;

import java.awt.Point;
import java.awt.geom.Rectangle2D;

import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridDimension;

public class PixelToCellPositionConverter {

  // MIGHT MAKE THIS CLASS
  // MIGHT NOT
  // DEPENDS ON THE NECESSITY
  private Rectangle2D box;
  private GridDimension gd;
  private double margin;
  
  /** 
  * @param box the box to draw the grid in
  * @param gd the grid dimension
  * @param margin the margin around cells
  */
  public PixelToCellPositionConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

   /**
  * @param cp the cell position as CellPosition-object
  * @return the bounds for the given cell as Rectangle2D-object
  */
  public CellPosition getCellForBounds(Point point) {
    double x = point.getX();
    double y = point.getY();

    x = Math.round(x/80);
    y = Math.round(x/80);



    double cellWidth = (box.getWidth() - (margin * (gd.cols()+1))) / gd.cols();
    double cellHeight = (box.getHeight() - (margin * (gd.rows()+1))) / gd.rows();
    double cellX = margin + box.getX() + (margin + cellWidth);
    double cellY = margin + box.getY() + (margin + cellHeight);
    return new CellPosition(0, 0);
  }
}
