package no.uib.inf101.chess.model;

import java.util.ArrayList;

import no.uib.inf101.chess.controller.ControllableChessModel;
import no.uib.inf101.chess.view.ViewableChessModel;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;

public class ChessModel implements ViewableChessModel, ControllableChessModel {
  /* +++++++++++++++ Field variables +++++++++++++++ */
  private ChessBoard board;
  private ArrayList<ChessPiece> whitePieces;
  private ArrayList<ChessPiece> blackPieces;
  private CellPosition selectedCell;
  
  
  /* +++++++++++++++ Constructor +++++++++++++++ */
  public ChessModel(ChessBoard board) {
    this.board = board;
  }
  
  @Override
  public GridDimension getDimension() {
    return board;
  }
  
  
  
  @Override
  public ArrayList<ChessPiece> getBlackPiecesOnBoard() {
    getPiecesOnBoard();
    return blackPieces;
  }
  
  @Override
  public ArrayList<ChessPiece> getWhitePiecesOnBoard() {
    getPiecesOnBoard();
    return whitePieces;
  }
  
  @Override
  public Iterable<GridCell<ChessPiece>> getPiecesOnBoard() {
    return board;
  }

  @Override
  public void resetBoard() {

  }

  @Override
  public ChessBoard getBoard() {
    return this.board;
  }
  
  public void generatePiece(ChessPiece piece, CellPosition pos) {
    piece.setPos(pos);
    this.board.set(pos, piece);
  }

  @Override
  public boolean movePiece(ChessPiece piece, CellPosition pos) {
    CellPosition currentPos = piece.getPos();
    if (piece.legalMove(board, currentPos, pos)) {
      piece.setPos(pos);
      board.set(pos, piece);
      board.set(currentPos, null);
      System.out.println("Legal move!");
      return true;
  } 
  System.out.println("Not legal move!");
  return false;
  }

  @Override
  public void setSelectedCell(CellPosition pos) {
    this.selectedCell = pos;
    System.err.println("Selected cell = " + this.selectedCell);
  }

  @Override
  public CellPosition getSelectedCell() {
    return this.selectedCell;
  }
}

  