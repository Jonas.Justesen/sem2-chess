package no.uib.inf101.chess.model;

import no.uib.inf101.grid.Grid;

// Grid copied from sem1 Tetris
public class ChessBoard extends Grid<ChessPiece> {

    public ChessBoard(int rows, int cols) {
        super(rows, cols);
    }
}
