package no.uib.inf101.chess.view;

// This class was imported from the previous lab 4. It has only been slightly modified to
// fit the new project. The only change is the package name and import directories.

import java.awt.geom.Rectangle2D;

import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridDimension;

public class CellPositionToPixelConverter {
  private Rectangle2D box;
  private GridDimension gd;
  private double margin;
  
  /** 
  * @param box the box to draw the grid in
  * @param gd the grid dimension
  * @param margin the margin around cells
  */
  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }
  
  /**
  * @param cp the cell position as CellPosition-object
  * @return the bounds for the given cell as Rectangle2D-object
  */
  public Rectangle2D getBoundsForCell(CellPosition cp) {
    double cellWidth = (box.getWidth() - (margin * (gd.cols()+1))) / gd.cols();
    double cellHeight = (box.getHeight() - (margin * (gd.rows()+1))) / gd.rows();
    double cellX = margin + box.getX() + (margin + cellWidth) * cp.col();
    double cellY = margin + box.getY() + (margin + cellHeight) * cp.row();
    return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
  }
}
