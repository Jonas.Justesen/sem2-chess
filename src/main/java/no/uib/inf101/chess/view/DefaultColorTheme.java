package no.uib.inf101.chess.view;

import java.awt.Color;

public class DefaultColorTheme implements ColorTheme {

    @Override
    public Character getPieceChar(Character c) {
        Character piece;
        piece = switch(c) {
            case 'k' -> '♚';
            case 'q' -> '♛';
            case 'r' -> '♜';
            case 'b' -> '♝';
            case 'n' -> '♞';
            case 'p' -> '♟';
            default -> throw new IllegalArgumentException("Character " + c + " is not a legal piece!");
        };
    return piece;
    }

    @Override
    public Color darkSquareColor() {
        return (new Color(102,76,40, 200));
    }

    @Override
    public Color lightSquareColor() {
        return (new Color(245, 245, 220, 200));
    }

    @Override
    public Color selectedCellColor() {
        return Color.YELLOW;
    }

    
}
