package no.uib.inf101.chess.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.chess.model.ChessPiece;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;

public class ChessView extends JPanel {
  private final GridDimension gd;
  private final int windowStartSizeX;
  private final int windowStartSizeY;
  private ViewableChessModel viewableChessModel;
  private final int INNER_MARGIN = 0;
  private final ColorTheme colorTheme;
  
  // Constructor
  public ChessView(ViewableChessModel viewableChessModel) {
    this.windowStartSizeX = 800;
    this.windowStartSizeY = 800;
    this.viewableChessModel = viewableChessModel;
    this.setFocusable(true);
    this.setPreferredSize(new Dimension(this.windowStartSizeX, this.windowStartSizeY)); // casting to int is not ideal, but will do for now
    this.gd = viewableChessModel.getDimension();
    this.colorTheme = new DefaultColorTheme();
  }
  
  // The paintComponent method is called by the Java Swing framework every time
  // either the window opens or resizes, or we call .repaint() on this object. 
  // Note: NEVER call paintComponent directly yourself
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGame(g2);
  }
  
  private void drawGame(Graphics2D g2) {
    double width = this.getWidth();
    double height = this.getHeight();
    
    CellPositionToPixelConverter cellPosition = new CellPositionToPixelConverter(new Rectangle2D.Double(0, 0, width, height), this.gd, INNER_MARGIN);
    drawBoard(g2, cellPosition);
    drawPieces(g2, cellPosition);
  }
  
  private void drawBoard(Graphics2D g2, CellPositionToPixelConverter cellPosition) {
    for (int i = 0; i < this.viewableChessModel.getDimension().rows(); i++){
      for (int j = 0; j < this.viewableChessModel.getDimension().cols(); j++){
        if ((i+j) % 2 == 0) {
          g2.setColor(colorTheme.lightSquareColor());
        } else {
          g2.setColor(colorTheme.darkSquareColor());
        }
        if (new CellPosition(i, j).equals(viewableChessModel.getSelectedCell())) {
          g2.setColor(colorTheme.selectedCellColor());
        }
        g2.fill(cellPosition.getBoundsForCell(new CellPosition(i, j)));
        g2.setColor(Color.GRAY);
        // debug: writes cellposition
        Inf101Graphics.drawCenteredString(g2, "("+i+", "+j+")", cellPosition.getBoundsForCell(new CellPosition(i, j)));
      }
    }    
  }
  // need to change to images with border
  private void drawPieces(Graphics2D g2, CellPositionToPixelConverter cellPosition) {
    for (GridCell<ChessPiece> piece : viewableChessModel.getPiecesOnBoard()) {
      if (piece.value() != null && !piece.value().isCaptured()) {
        g2.setFont(new Font("Arial", Font.BOLD, 100));
        String pieceChar = Character.toString(colorTheme.getPieceChar(piece.value().pieceChar()));
        double cellPosX = cellPosition.getBoundsForCell(piece.pos()).getCenterX() - 8;
        double cellPosY = cellPosition.getBoundsForCell(piece.pos()).getCenterY() - 9;
        if (piece.value().isWhite()) {
          g2.setColor(Color.WHITE);
        } else {
          g2.setColor(Color.BLACK);
        }
        Inf101Graphics.drawCenteredString(g2, pieceChar, cellPosX, cellPosY);
      }
    }
  }
}
