package no.uib.inf101.chess.view;

import java.awt.Color;

public interface ColorTheme {
  
  /**
  * Takes a character and returns the UTF-8 piece associated with that character
  * @param c piece as character (king = 'k')
  * @return Character
  */
  Character getPieceChar (Character c);

  /**
   * Color designated for the dark fields on board
   * @return
   */
  Color darkSquareColor();

  /**
   * Color designated for the light fields on board
   * @return
   */
  Color lightSquareColor();
  
  Color selectedCellColor();
}
