package no.uib.inf101.chess.view;

import java.util.ArrayList;

import no.uib.inf101.chess.model.ChessPiece;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;

public interface ViewableChessModel {
    /**
  * @return The dimension of the grid as GridDimension object
  */
  GridDimension getDimension();
  
  /**
  * @return Iterable of cells with white chesspieces
  */
  ArrayList<ChessPiece> getWhitePiecesOnBoard();

  /**
  * @return Iterable of cells with black chesspieces
  */
  ArrayList<ChessPiece> getBlackPiecesOnBoard();

  /**
  * @return Iterable of cells on board
  */
  Iterable<GridCell<ChessPiece>> getPiecesOnBoard();

  public CellPosition getSelectedCell();
}
