package no.uib.inf101.chess.controller;

import java.awt.event.MouseEvent;

import no.uib.inf101.chess.model.ChessPiece;
import no.uib.inf101.chess.view.ChessView;
import no.uib.inf101.grid.CellPosition;

public class ChessController implements java.awt.event.MouseListener {
    private ChessView chessView;
    private ControllableChessModel controllableChessModel;
    private ChessPiece piece;

    public ChessController(ChessView chessView, ControllableChessModel controllableChessModel) {
        this.chessView = chessView;
        this.controllableChessModel = controllableChessModel;
        this.piece = controllableChessModel.getBoard().get(new CellPosition(7, 4));
        this.chessView.addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        CellPosition cellClicked = getCellFromPixel(e);

        // Four possibilities
        // 1. selected cell = 0 -> set selected cell = clicked cell
        // 2. selected cell = clicked cell -> set selected cell = 0
        // 3. selected cell != 0 but previous selected cell = null -> set selected cell = clicked cell
        // 4. selected cell != 0 and previous cell != null
        if (controllableChessModel.getSelectedCell() == null) {
            controllableChessModel.setSelectedCell(cellClicked);
        }
        else if (controllableChessModel.getSelectedCell().equals(cellClicked)) {
            controllableChessModel.setSelectedCell(null);
        }
        else if (controllableChessModel.getBoard().get(controllableChessModel.getSelectedCell()) == null) {
            controllableChessModel.setSelectedCell(cellClicked);
        }
        else if (controllableChessModel.getBoard().get(controllableChessModel.getSelectedCell()) != null) {
            controllableChessModel.movePiece(controllableChessModel.getBoard().get(controllableChessModel.getSelectedCell()), cellClicked);
            controllableChessModel.setSelectedCell(null);
        }
        // remove after debug
        System.out.println(cellClicked);
        this.chessView.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    private CellPosition getCellFromPixel(MouseEvent e) {
        double x = e.getX();
        double y = e.getY();

        x = (int) ((Math.floor(x)) / 100);
        y = (int) ((Math.floor(y)) / 100);

        int i = (int) y;
        int j = (int) x;

        return new CellPosition(i, j);
    }
}
