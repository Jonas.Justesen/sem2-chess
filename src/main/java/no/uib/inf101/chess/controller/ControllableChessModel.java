package no.uib.inf101.chess.controller;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessPiece;
import no.uib.inf101.grid.CellPosition;

public interface ControllableChessModel {
    /**
     * Resets the chessboard to its original state
     */
    public void resetBoard();

    public ChessBoard getBoard();

    public boolean movePiece(ChessPiece piece, CellPosition pos);

    public void setSelectedCell(CellPosition pos);

    public CellPosition getSelectedCell();
}
