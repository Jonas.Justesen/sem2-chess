package no.uib.inf101.chess;

import javax.swing.JFrame;

import no.uib.inf101.chess.controller.ChessController;
import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessModel;
import no.uib.inf101.chess.model.pieces.King;
import no.uib.inf101.chess.view.ChessView;
import no.uib.inf101.grid.CellPosition;

public class chessMain {
    public static final String WINDOW_TITLE = "Checkers or Chess";

    public static void main(String[] args) throws InterruptedException {
        ChessBoard board = new ChessBoard(8,8);
        ChessModel model = new ChessModel(board);
        ChessView view = new ChessView(model);
        @SuppressWarnings("unused")
        ChessController controller = new ChessController(view, model);
        
        model.generatePiece(new King(model, true), new CellPosition(7, 4));

        JFrame frame = new JFrame(WINDOW_TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setContentPane(view);
        frame.pack();
        frame.setVisible(true);
    }
}
